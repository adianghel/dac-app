<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateReservationsTable extends Migration
{
    public function up()
    {
      if (!Schema::hasColumn('dac_webapp_reservations', 'status')) {
        Schema::table('dac_webapp_reservations', function ($table) {
          $table->enum('status', ['active', 'pending', 'archive', 'deleted']);
        });
      }
      if (!Schema::hasColumn('dac_webapp_reservations', 'last_edited_by')) {
        Schema::table('dac_webapp_reservations', function ($table) {
          $table->string('last_edited_by', 100);
        });
      }
    }

    public function down()
    {
        //Schema::dropIfExists('dac_webapp_reservations');
    }
}

