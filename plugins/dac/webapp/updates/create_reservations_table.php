<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateReservationsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('dac_webapp_reservations')) {
            Schema::create('dac_webapp_reservations', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->dateTime('appointment');
                $table->string('from');
                $table->string('to');
                $table->string('email');
                $table->text('notes');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        //Schema::dropIfExists('dac_webapp_reservations');
    }
}
