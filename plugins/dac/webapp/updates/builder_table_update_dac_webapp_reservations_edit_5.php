<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDacWebappReservationsEdit5 extends Migration
{
    public function up()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->integer('edited_by');
        });
    }
    
    public function down()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->dropColumn('edited_by');
        });
    }
}
