<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDacWebappReservations extends Migration
{
    public function up()
    {
        Schema::table('dac_webapp_reservations', function($table)
        {
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::table('dac_webapp_reservations', function($table)
        {
            $table->dropColumn('name');
        });
    }
}
