<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDacWebappReservationsEdit extends Migration
{
    public function up()
    {
        Schema::create('dac_webapp_reservations_edit', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dac_webapp_reservations_edit');
    }
}
