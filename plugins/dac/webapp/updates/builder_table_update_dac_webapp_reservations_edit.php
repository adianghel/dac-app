<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDacWebappReservationsEdit extends Migration
{
    public function up()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->integer('id')->unsigned()->change();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->dropPrimary(['id']);
            $table->integer('id')->unsigned(false)->change();
        });
    }
}
