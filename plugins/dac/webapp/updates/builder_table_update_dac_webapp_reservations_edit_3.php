<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDacWebappReservationsEdit3 extends Migration
{
    public function up()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->integer('order_id');
        });
    }
    
    public function down()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->dropColumn('order_id');
        });
    }
}
