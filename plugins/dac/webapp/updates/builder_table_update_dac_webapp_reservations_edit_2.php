<?php namespace Dac\WebApp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDacWebappReservationsEdit2 extends Migration
{
    public function up()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('dac_webapp_reservations_edit', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
