
$(document).ready(function () {
  //disable stuur offerte
  $('#Form-field-Reservation-send_offerte').attr('checked', false);
  $("#Form-field-Reservation-pakket").on('change', function () {
    var mainVal = $(this).val();
    switch (mainVal) {
      case 'Custom':
        $('#Form-field-Reservation-verhuizers, #Form-field-Reservation-lift, #Form-field-Reservation-bakwagens, #Form-field-Reservation-vakwagen').removeClass('fieldChanged').attr('disabled', false);
        break;
      case 'Actie pakket 1':
        $('#Form-field-Reservation-verhuizers').val(2).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-lift').val('1').addClass('fieldChanged').attr("disabled", true);
        $('#Form-field-Reservation-bakwagens').val(1).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-vakwagen').val(0).addClass('fieldChanged').attr('disabled', true);
        break;
      case 'Actie pakket 2':
        $('#Form-field-Reservation-verhuizers').val(2).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-lift').val('1').addClass('fieldChanged').attr("disabled", true);
        $('#Form-field-Reservation-bakwagens').val(1).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-vakwagen').val(0).addClass('fieldChanged').attr('disabled', true);
        break;
      case 'Actie pakket 3':
        $('#Form-field-Reservation-verhuizers').val(1).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-lift').val('1').addClass('fieldChanged').attr("disabled", true);
        $('#Form-field-Reservation-bakwagens').val(0).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-vakwagen').val(0).addClass('fieldChanged').attr('disabled', true);
        break;
      case 'Actie pakket 4':
        $('#Form-field-Reservation-verhuizers').val(1).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-lift').val('1').addClass('fieldChanged').attr("disabled", true);
        $('#Form-field-Reservation-bakwagens').val(0).addClass('fieldChanged').attr('disabled', true);
        $('#Form-field-Reservation-vakwagen').val(0).addClass('fieldChanged').attr('disabled', true);
        break;
    }
  });
});