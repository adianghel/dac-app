<?php namespace Dac\WebApp\Models;

use Model;
use \October\Rain\Database\SoftDeletes;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DB;
use Flash;


/**
 * Reservation Model
 */
class Reservation extends Model
{
    use \Jacob\Logbook\Traits\LogChanges;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'dac_webapp_reservations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = ['planning' => ['Dac\WebApp\Models\ReservationPlanning', 'key' => 'order_id']];
    public $hasMany = [
        //'edits' => ['Dac\WebApp\Models\ReservationEdits', 'key' => 'order_id'],
        
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    /*$edits = Reservation::find(1)->edits;
        var_dump($edits);
        foreach ($edits as $edit) {
        }
    */
    /**
     * If you override this function you can change the column name that is displayed in the log book
     * The returned column will be translated if it is possible
     *
     * @param string $column
     * @return string
     */
    public static function changeLogBookDisplayColumn($column)
    {
        if($column == 'fromStreet') {
            return 'Straat';
        } else if($column == 'fromStreetNr') {
            return 'Nmmr';
        } else if($column == 'fromEtage') {
            return 'Van Etage';
        } else if($column == 'notes') {
            return 'Opmerkingen klant';
        } else {
            return $column;
        }
    }

    //public $logBookModelName = 'Edits2';
    public function afterSave() {
        //throw new \Exception("Invalid Model!");
        if ($this->send_offerte) {
            //DB::table('dac_webapp_reservations')->where('id', $reservation->id)->update(['send_offerte' => 'sent']);
            // Collect input
            //$name = 'Adi';
            //$email = 'adi@dare2host.com';
            //$message = 'test';

            // Submit form
            //$to = System\Models\MailSettings::get('sender_email');
            //$params = compact('name','email');
            //Mail::sendTo($to, 'temp.website::mail.newrequest', $params);
            //return true;
            

            $data = [
                'email'   => $this->email,
                'name'    => $this->name,
                'package' => $this->pakket,
                'verhuizers' => $this->verhuizers,
                'lift' => $this->lift,
                'bakwagens' => $this->bakwagens,
                'vakwagen' => $this->vakwagen,
                'extra_uren' => $this->extra_hours,
                'extra_km' => $this->extra_km,
                //'subject' => $request->input('subject'),
                //'body'    => $request->input('body')
            ];
            Mail::send('send.offerte', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['name']);
            });
            Flash::success('Aanbieding opgeslagen en e-mail verzenden!');
        } else {
            //throw new \Exception("Invalid Model!");
        }
        /** create planing */
        //if($this->)
    }
    /** @var array $ignoreFields fields to ignore */
    protected $ignoreFieldsLogbook = [
        'send_offerte',
        'updated_at',
    ];
    /** filter list dates */
    public function scopeCreatedDaysAgo($query, $days = 1)
    {
        return $query->whereDate('appointment', '=', Carbon::today());
    }
}   