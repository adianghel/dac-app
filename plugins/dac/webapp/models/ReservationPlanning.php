<?php namespace Dac\WebApp\Models;

use Model;
use Db;

/**
 * Model
 */
class ReservationPlanning extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'dac_webapp_reservations_planning';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsTo = [
                    'reservation' => [
                         'Dac\WebApp\Models\Reservation',
                      ],
                 ];
      /** repeater fields json */
    protected $jsonable = ['selected_movers', 'vrachtwagen', 'bakwagen', 'lift']; 
    
    public function getSerialOptions() {
          $records = Db::table('dac_webapp_plannings')->where('record_type', 'Lift')->lists('lift_serial', 'lift_serial');
          return $records;
          // Or return ManufacturerModel::all()->pluck('name','id');
     }
     public function getVcarsOptions() {
          $records = Db::table('dac_webapp_plannings')->where('record_type', 'Vrachtwagen')->lists('truck_license', 'truck_license');
          return $records;
     }
     public function getBcarsOptions() {
          $records = Db::table('dac_webapp_plannings')->where('record_type', 'Bakwagen')->lists('truck_license', 'truck_license');
          return $records;
     }
     public function getMoversOptions() {
          $records = Db::table('dac_webapp_plannings')->where('record_type', 'Verhuizer')->lists('verhuizer_name', 'verhuizer_name');
          return $records;
     }
}
