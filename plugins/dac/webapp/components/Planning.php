<?php namespace Dac\WebApp\Components;

use Cms\Classes\ComponentBase;

class Planning extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Planning Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
