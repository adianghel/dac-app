<?php namespace Dac\WebApp\Components;

use Cms\Classes\ComponentBase;

class Reservation extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Reservation Component',
            'description' => 'Dac Reservations WebAPP'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
