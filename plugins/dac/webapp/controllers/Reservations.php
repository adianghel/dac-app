<?php namespace Dac\WebApp\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend;
use BackendAuth;
use DB;
use Carbon\Carbon;
use Session;
use Input;
use Redirect;
use SnappyPDF;
use Flash;
/** facade today plan pdf */
use Renatio\DynamicPDF\Classes\PDF; 

/**
 * Reservations Back-end Controller
 */
class Reservations extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Dac.WebApp', 'webapp', 'reservations');


        //add custom js and css
        $this->addCss("/plugins/dac/webapp/assets/css/custom.css", "1.0.0");
        $this->addJs("/plugins/dac/webapp/assets/js/custom.js", "1.0.0");

        $this->relationConfig = $this->mergeConfig($this->relationConfig, '$/luketowers/easyaudit/yaml/relation.activities.yaml');
    }
    //custom html code index
    public function index()
    {
        //
        // Do any custom code here
        //

        // Call the ListController behavior index() method
        $this->asExtension('ListController')->index();
    }
    //inject classes in lists rows
    public function listInjectRowClass($reservation, $definition)
    {
        // Strike deteled reservations
        if ($reservation->status=='Geaccepteerd') {
           return 'positive';
        } else if ($reservation->status=='Geannulleerd') {
            return 'negative';
        } else if ($reservation->status=='Wacht op bevestiging') {
            return 'important';
        }
    }
    //save current user to last_edit_by
    public function formBeforeSave($reservation){
        if(!is_null($reservation->id)) {
            $user = BackendAuth::getUser();
            $uName = $user->first_name.' '.$user->last_name;
            $reservation->last_edited_by= $uName;
            DB::table('dac_webapp_reservations_edit')->insert([
                ['order_id' => $reservation->id, 'edited_by' => $uName]
            ]);
        }
        if(is_null($reservation->rtype)) { 
            DB::table('dac_webapp_reservations')->where('id', $reservation->id)->update(['rtype' => 'Manueel']);
        }
    }

    /** generate today plan pdf */
    public function onDownloadTodayPDF()
    {
        $todayReservations = Db::table('dac_webapp_reservations')
                            ->whereDate('appointment', '=', Carbon::today())
                            ->join('dac_webapp_reservations_planning', 'dac_webapp_reservations.id', '=', 'dac_webapp_reservations_planning.order_id')
                            ->get();
        //$planningDetails = Db::table('dac_webapp_plannings')->get();
        /** repeat for testing */
        foreach($todayReservations as $reservation) {
            array_walk_recursive($reservation, function (&$item, $key) {
               if ($key == 'selected_movers') {
                   $item = json_decode($item);
                    $item = implode(",<br />", array_map(function ($entry) {
                        return $entry->movers;
                    }, $item));
                } else if ($key == 'vrachtwagen') {
                   $item = json_decode($item);
                    $item = implode(",<br />", array_map(function ($entry) {
                        return $entry->vcars;
                    }, $item));
                } else if ($key == 'bakwagen') {
                   $item = json_decode($item);
                    $item = implode(",<br />", array_map(function ($entry) {
                        return $entry->bcars;
                    }, $item));
                } else if ($key == 'lift') {
                   $item = json_decode($item);
                    $item = implode(",<br />", array_map(function ($entry) {
                        return $entry->serial;
                    }, $item));
                }
            });
        }

        $templateCode = 'reservations.today'; // unique code of the template
        $data = ['name' => 'Adi Anghel', 'reservations' => $todayReservations]; // optional data used in template
        
        /** check if reservations today and return pdf or error */
        if(sizeof($todayReservations) > 0) {
            PDF::loadTemplate($templateCode, $data)->setPaper('a4', 'landscape')->save('plugins/dac/webapp/assets/pdfdownload/today_download.pdf');
            return Redirect::to('plugins/dac/webapp/assets/pdfdownload/today_download.pdf');
        } else {
            Flash::error('No reservations today!');
        }
    }
}
