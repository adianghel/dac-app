<?php namespace Dac\WebApp\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Planning Back-end Controller
 */
class Planning extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Dac.WebApp', 'webapp', 'planning');
    }
}
