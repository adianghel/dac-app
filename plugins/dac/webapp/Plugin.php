<?php namespace Dac\WebApp;

use Backend;
use Event;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * WebApp Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'WebApp',
            'description' => 'DacNV reservations module',
            'author'      => 'Dac',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Dac.WebApp', 'reservations', '@/plugins/dac/webapp/reservations/_sidebar.htm');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        //remove cms and media from backend
        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->removeMainMenuItem('October.Cms', 'cms');
            $manager->removeMainMenuItem('October.Backend', 'media');

        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
            'Dac\WebApp\Components\Reservation' => 'Reservation',
            'Dac\WebApp\Components\Planning' => 'Planning'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        //return []; // Remove this line to activate

        return [
            'dac.webapp.some_permission' => [
                'tab' => 'WebApp',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'webapp' => [
                'label'       => 'Dac WebApp',
                'url'         => Backend::url('dac/webapp/reservations'),
                'icon'        => 'icon-truck',
                'permissions' => ['dac.webapp.*'],
                'order'       => 100,
            ],
            'webapp-planning' => [
                'label'       => 'Dac Planning',
                'url'         => Backend::url('dac/webapp/planning'),
                'icon'        => 'icon-address-book',
                'permissions' => ['dac.webapp.*'],
                'order'       => 101,
            ],
        ];
    }
}
